package main

import (
	"context"
	"fmt"

	"gitlab.com/nghianguyendnse/permit-sdk/permit-golang/pkg/config"
	"gitlab.com/nghianguyendnse/permit-sdk/permit-golang/pkg/permit"
)

func main() {

	// Building new config for Permit client
	cfg, err := config.NewPermitConfig()
	if err != nil {
		fmt.Print("NewPermitConfig error: ", err)
	}

	// This line initializes the SDK and connects your Go app
	permitClient := permit.NewClient(
		//Add your namespace here ('default' is the default value)
		cfg.WithNamespace("finoffer").
			WithContext(context.Background()). //The default context is context.Background()
			Build(),
	)
	permitted, err := permitClient.Check("test@dnse.com.vn", "create", "symbol")
	if err != nil {
		fmt.Print(err)
		return
	}
	if permitted {
		fmt.Print("Result is PERMITTED to perform the action on the resources")
	} else {
		fmt.Print("Result is NOT PERMITTED to perform the action on the resources")
	}
}

package config

import (
	"context"
	"errors"
	"strings"

	"github.com/knadh/koanf/v2"

	string_helper "gitlab.com/nghianguyendnse/permit-sdk/permit-golang/string-helper"

	"github.com/knadh/koanf/parsers/yaml"
	"github.com/knadh/koanf/providers/env"
	"github.com/knadh/koanf/providers/fs"
)

const (
	EnvPrefix = "APP__"
)

type PermitConfig struct {
	Url       string `koanf:"url"`
	Namespace string `koanf:"namespace"`
	Context   context.Context
}

func NewPermitConfig() (PermitConfig, error) {
	var config PermitConfig
	k := koanf.New(".")
	configProvider := fs.Provider(EmbeddedFiles, "config.yaml")
	if err := k.Load(configProvider, yaml.Parser()); err != nil {
		return config, errors.New("cannot read config from file")
	}
	if err := k.Load(
		env.Provider(
			EnvPrefix, ".", func(s string) string {
				return string_helper.SnakeToCamel(
					strings.Replace(
						strings.ToLower(
							strings.TrimPrefix(s, EnvPrefix),
						), "__", ".", -1,
					),
				)
			},
		), nil,
	); err != nil {
		return config, err
	}
	if err := k.Unmarshal("", &config); err != nil {
		return config, err
	}
	config.Context = context.Background()
	return config, nil
}

func (c *PermitConfig) WithUrl(url string) *PermitConfig {
	c.Url = url
	return c
}

func (c *PermitConfig) WithNamespace(namespace string) *PermitConfig {
	c.Namespace = namespace
	return c
}

func (c *PermitConfig) WithContext(ctx context.Context) *PermitConfig {
	c.Context = ctx
	return c
}

func (c *PermitConfig) Build() PermitConfig {
	return *c
}

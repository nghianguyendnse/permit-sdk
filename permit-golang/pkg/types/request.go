package types

type VerifyActionRequest struct {
	UserEmail string `json:"userEmail"`
	ActionId  int64  `json:"actionId"`
}

type VerifyActionDomainRequest struct {
	UserEmail string `json:"userEmail"`
	Domain    string `json:"domain"`
	Action    string `json:"action"`
	Model     string `json:"model"`
}

package types

type ErrorResponse struct {
	Status     int    `json:"status"`
	Code       string `json:"code"`
	Message    string `json:"message"`
	Error      string `json:"error"`
	ErrorTrace string `json:"ErrorTrace"`
}

package tests

import (
	"context"
	"testing"

	"gitlab.com/nghianguyendnse/permit-sdk/permit-golang/pkg/types"

	"gitlab.com/nghianguyendnse/permit-sdk/permit-golang/pkg/config"
	"gitlab.com/nghianguyendnse/permit-sdk/permit-golang/pkg/permit"

	"github.com/h2non/gock"
	"github.com/stretchr/testify/assert"
)

func TestClient_Check(t *testing.T) {
	defer gock.Off()
	permitConfig, _ := config.NewPermitConfig()
	permitClient := permit.NewClient(permitConfig.WithNamespace("namespacetest").
		WithContext(context.Background()).Build(),
	)

	t.Run(
		"check permission success", func(t *testing.T) {
			gock.New(permitConfig.Url).
				Post("").
				Reply(200).
				JSON(
					types.VerifyActionResponse{
						Data: true,
					},
				)
			permitted, err := permitClient.Check("dnse.admin@dnse.com.vn", "create", "symbol")
			assert.Nil(t, err)
			assert.Equal(t, true, permitted)
		},
	)

	t.Run(
		"check permission unauthorized", func(t *testing.T) {
			gock.New(permitConfig.Url).
				Post("").
				Reply(401).
				JSON(
					types.ErrorResponse{
						Status:     401,
						ErrorTrace: "Unauthorized",
					},
				)
			_, err := permitClient.Check("dnse.admin@dnse.com.vn", "create", "symbol")
			assert.ErrorContains(t, err, "Unauthorized")
		},
	)

	t.Run(
		"check permission not exist", func(t *testing.T) {
			gock.New(permitConfig.Url).
				Post("").
				Reply(404).
				JSON(
					types.ErrorResponse{
						Status: 404,
						Error:  "not found resources",
					},
				)
			_, err := permitClient.Check("dnse.admin@dnse.com.vn", "create", "symbol")
			assert.ErrorContains(t, err, "not found resources")
		},
	)

	t.Run(
		"check permission invalid request", func(t *testing.T) {
			gock.New(permitConfig.Url).
				Post("").
				Reply(400).
				JSON(
					types.ErrorResponse{
						Status:  400,
						Message: "invalid payload",
					},
				)
			_, err := permitClient.Check("dnse.admin@dnse.com.vn", "create", "symbol")
			assert.ErrorContains(t, err, "invalid payload")
		},
	)
}

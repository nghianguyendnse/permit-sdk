package permit

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/nghianguyendnse/permit-sdk/permit-golang/pkg/config"
	"gitlab.com/nghianguyendnse/permit-sdk/permit-golang/pkg/types"
)

type Client struct {
	httpClient *http.Client
	config     config.PermitConfig
}

func NewClient(config config.PermitConfig) *Client {
	return &Client{
		config: config,
		httpClient: &http.Client{
			Timeout: 5 * time.Second,
		},
	}
}

func (c *Client) NewRequest(ctx context.Context, method, url string, body any) (*http.Request, error) {
	marshaledBody, err := json.Marshal(body)
	if err != nil {
		return nil, fmt.Errorf("NewRequest %w", err)
	}
	req, err := http.NewRequestWithContext(ctx, method, url, bytes.NewBuffer(marshaledBody))
	if err != nil {
		return nil, fmt.Errorf("NewRequest %w", err)
	}
	req.Header.Set("Content-Type", "application/json")
	return req, nil
}

func (c *Client) Check(email, action, model string) (bool, error) {
	req, err := c.NewRequest(
		c.config.Context,
		http.MethodPost,
		c.config.Url,
		types.VerifyActionDomainRequest{
			UserEmail: email,
			Domain:    c.config.Namespace,
			Action:    action,
			Model:     model,
		},
	)
	if err != nil {
		return false, fmt.Errorf("check newrequest: %w", err)
	}
	res, err := c.httpClient.Do(req)
	if err != nil {
		return false, fmt.Errorf("check http do: %w", err)
	}
	defer func() {
		if scopedErr := res.Body.Close(); scopedErr != nil {
			err = fmt.Errorf("check %w", scopedErr)
		}
	}()
	if res.StatusCode != http.StatusOK {
		errorResponse := types.ErrorResponse{}
		_ = json.NewDecoder(res.Body).Decode(&errorResponse)
		if errorResponse.Message != "" {
			return false, fmt.Errorf(
				"check got error Status %d, Message: %s", res.StatusCode, errorResponse.Message,
			)
		}
		if errorResponse.Error != "" {
			return false, fmt.Errorf(
				"check got error Status %d, Error: %s", res.StatusCode, errorResponse.Error,
			)
		}
		if errorResponse.ErrorTrace != "" {
			return false, fmt.Errorf(
				"check got error Status %d, ErrorTrace: %s", res.StatusCode, errorResponse.ErrorTrace,
			)
		}
	}
	var dest types.VerifyActionResponse
	if err := json.NewDecoder(res.Body).Decode(&dest); err != nil {
		return false, fmt.Errorf("check %w", err)
	}
	return dest.Data, nil
}

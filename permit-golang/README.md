# permit-golang


## Overview

This package sdk provides a Golang SDK for accessing the g2fina API Verify permission.
This package provides a simple and easy-to-use interface for interacting with the API. It handles authentication,
request/response serialization and deserialization, and error handling, allowing you to focus on building your
application logic.

## Installation:
To use the SDK, you can use go get to install the SDK:
```
go get gitlab.com/nghianguyendnse/permit-sdk/permit-golang
```
Put the package under your project folder and add the following in import:
```
import(
    "gitlab.com/nghianguyendnse/permit-sdk/permit-golang/pkg/config"
	"gitlab.com/nghianguyendnse/permit-sdk/permit-golang/pkg/permit"
)
```

## Usage:
### Init the SDK 
To init the SDK, you need to create a new Permit client with the namespace you want to use, 
and the context in which you are working.

First we will create a new Config object so we can pass it to the Permit client.

Second, we will create a new Permit client with the Config object we created.

```
package main

import(
    "gitlab.com/nghianguyendnse/permit-sdk/permit-golang/pkg/config"
	"gitlab.com/nghianguyendnse/permit-sdk/permit-golang/pkg/permit"
)

func main() {
    // Building new config for Permit client
	cfg, _ := config.NewPermitConfig()

	// This line initializes the SDK and connects your Go app
	permitClient := permit.NewClient(
		//Add your namespace here ('default' is the default value)
		cfg.WithNamespace("finoffer").
		    WithContext(context.Background()).
		    Build(),
	)
}
```

### Check for permissions
To check permissions using our Permit.Check() method, you will have to pass in a string representing the email 
of the user you want to check permissions for, a string representing the action you want to check for, 
and a string representing the resource you want to check for.

```
package main

import (
	"context"
	"fmt"

	"gitlab.com/nghianguyendnse/permit-sdk/permit-golang/pkg/config"
	"gitlab.com/nghianguyendnse/permit-sdk/permit-golang/pkg/permit"
)

func main() {

	// Building new config for Permit client
	cfg, err := config.NewPermitConfig()
	if err != nil {
		fmt.Print("NewPermitConfig error: ", err)
	}

	// This line initializes the SDK and connects your Go app
	permitClient := permit.NewClient(
		//Add your namespace here ('default' is the default value)
		cfg.WithNamespace("finoffer").
		    WithContext(context.Background()).
		    Build(),
	)
	permitted, err := permitClient.Check("YOUR EMAIL HERE", "create", "symbol")
	if err != nil {
		return
	}
	if permitted {
		// Let the user do something with the resource
	} else {
		// Deny the user
	}
}
```

